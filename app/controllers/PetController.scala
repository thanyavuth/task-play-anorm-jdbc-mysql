package controllers

import javax.inject.Inject

import play.api.db._
import play.api.mvc._
import anorm._
import play.api.libs.json._

import scala.concurrent.Future

class PetController @Inject()(db: Database, cc: ControllerComponents) extends AbstractController(cc) {

  case class Pet(name: String, owner: String, species: String, sex: String, birth: String, death: String)

  def allPet = Action {
    val response: JsValue = db.withConnection { implicit c =>
      //      val result: Boolean = SQL("SELECT * FROM pet").execute()

      val parser: RowParser[Map[String, String]] =
        SqlParser.folder(Map.empty[String, String]) { (map, value, meta) =>
          Right(map + (meta.column.qualified -> value.toString))
        }

      val result: List[Map[String, String]] = SQL"SELECT * FROM pet".as(parser.*)

      println(result)
      println(Json.toJson(result))

      Json.toJson(result)
    }

    Ok(response)
  }

  def newPet = Action.async(parse.json) { request =>
    val requestData = request.body
    implicit val petReads = Json.reads[Pet]
    val petFromJson = Json.fromJson[Pet](requestData).get

    val response = db.withConnection { implicit c =>
      val parser: RowParser[Map[String, String]] =
        SqlParser.folder(Map.empty[String, String]) { (map, value, meta) =>
          Right(map + (meta.column.qualified -> value.toString))
        }

      val result: Option[Long] =
        SQL("INSERT INTO pet VALUES ({name}, {owner}, {species}, {sex}, {birth}, {death})")
          .on(
            'name -> petFromJson.name,
            'owner -> petFromJson.owner,
            'species -> petFromJson.species,
            'sex -> petFromJson.sex,
            'birth -> petFromJson.birth,
            'death -> petFromJson.death)
          .executeInsert()

      println(result)
      println(Json.toJson(result))

      Json.toJson(result)
    }
    Future.successful(Ok(response))
  }


  def editPet = TODO

  def killPet = TODO
}