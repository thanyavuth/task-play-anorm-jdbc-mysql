package controllers

import javax.inject.Inject

import play.api.db._
import play.api.mvc._
import anorm._
import play.api.libs.json._

import scala.concurrent.Future

class TaskController @Inject()(db: Database, cc: ControllerComponents) extends AbstractController(cc) {

  case class Task(title: String, detail: String, status: String)


  val parser: RowParser[Map[String, String]] =
    SqlParser.folder(Map.empty[String, String]) { (map, value, meta) =>
      Right(map + (meta.column.qualified -> value.toString))
    }


  def getResponse(x: JsValue) = x.toString() match {
    case "0" => Future.successful(BadRequest(""))
    case "1" => Future.successful(Ok(""))
  }


  def allTask = Action {
    val response: JsValue = db.withConnection { implicit c =>
      val result: List[Map[String, String]] = SQL"SELECT * FROM Task".as(parser.*)
      Json.toJson(result)
    }

    Ok(response)
  }


  def someTask(task_id: String) = Action {
    val response: JsValue = db.withConnection { implicit c =>
      val result: List[Map[String, String]] =
        SQL("SELECT * FROM Task WHERE id={id}")
          .on('id -> task_id)
          .as(parser.*)
      Json.toJson(result)
    }

    Ok(response.head.get.toString)
  }


  def newTask = Action.async(parse.json) { request =>
    val requestData = request.body
    implicit val taskReads = Json.reads[Task]
    val taskFromJson = Json.fromJson[Task](requestData).get

    val response = db.withConnection { implicit c =>
      val result =
        SQL("INSERT INTO Task(Title, Detail, Status) VALUES ({title}, {detail}, {status})")
          .on(
            'title -> taskFromJson.title,
            'detail -> taskFromJson.detail,
            'status -> taskFromJson.status)
          .executeInsert()
      Json.toJson(result)
    }

    Future.successful(Created(response))
  }


  def editTask(task_id: String) = Action.async(parse.json) { request =>
    val requestData = request.body
    implicit val taskReads = Json.reads[Task]
    val taskFromJson = Json.fromJson[Task](requestData).get

    val response = db.withConnection { implicit c =>
      val result =
        SQL("UPDATE Task SET Title={title}, Detail={detail}, Status={status} WHERE id={id}")
          .on(
            'id -> task_id,
            'title -> taskFromJson.title,
            'detail -> taskFromJson.detail,
            'status -> taskFromJson.status)
          .executeUpdate()
      Json.toJson(result)
    }

    getResponse(response)
  }


  def delTask(task_id: String) = Action.async(parse.json) { request =>
    val requestData = request.body
    implicit val taskReads = Json.reads[Task]
    val taskFromJson = Json.fromJson[Task](requestData).get

    val response = db.withConnection { implicit c =>
      val result =
        SQL("DELETE FROM Task WHERE id={id}")
          .on(
            'id -> task_id)
          .executeUpdate()
      Json.toJson(result)
    }

    getResponse(response)
  }
}