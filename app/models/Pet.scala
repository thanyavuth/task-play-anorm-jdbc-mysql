package models

import java.sql.Date

/**
  * Created by thanyavuth on 13/7/2560.
  */

case class Pet(id: Long, name: String, owner: String, species: String, sex: String, birth: Date, death: Date)

object Pets {

  var pets: Seq[Pet] = Seq()

  def add(user: Pet): String = {
    pets = pets :+ user.copy(id = pets.length) // manual id increment
    "User successfully added"
  }

  def delete(id: Long): Option[Int] = {
    val originalSize = pets.length
    pets = pets.filterNot(_.id == id)
    Some(originalSize - pets.length) // returning the number of deleted users
  }

  def get(id: Long): Option[Pet] = pets.find(_.id == id)

  def listAll: Seq[Pet] = pets

}